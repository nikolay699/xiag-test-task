//
// Created by Nikolay on 02.06.17.
// Copyright (c) 2017 Nikolay. All rights reserved.
//

#import "ImageTableViewCell.h"
#import "Item.h"
#import "NetworkManager.h"

@implementation ImageTableViewCell

- (void)configureWithItem:(Item *)item {

    self.textLabel.text = item.name;

    NSURL *imageURL = [[NSURL alloc] initWithString:item.thumbnailImageURL];
    [[NetworkManager sharedManager] loadDataFromURL:imageURL completion:^(NSData *data, NSError *error) {

        UIImage *image = [[UIImage alloc] initWithData:data];

        dispatch_async(dispatch_get_main_queue(), ^{
            if (image) {
                [self.imageView setImage:image];
            } else {
                [self.imageView setImage:[UIImage imageNamed:@"NoImage"]];
            }
        });
    }];
}

@end