//
// Created by Nikolay on 02.06.17.
// Copyright (c) 2017 Nikolay. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class Item;

@interface ImageTableViewCell : UITableViewCell

- (void)configureWithItem:(Item *)item;

@end