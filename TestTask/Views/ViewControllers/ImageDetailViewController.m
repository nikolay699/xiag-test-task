//
// Created by Nikolay on 02.06.17.
// Copyright (c) 2017 Nikolay. All rights reserved.
//

#import "ImageDetailViewController.h"
#import "Item.h"
#import "NetworkManager.h"
#import <MessageUI/MessageUI.h>

@interface ImageDetailViewController() <MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *noContentView;
@property (strong, nonatomic) MFMailComposeViewController *mailComposeViewController;

@end

@implementation ImageDetailViewController

#pragma mark - Life cycle

- (void)viewDidLoad {

    [super viewDidLoad];
    [self loadData];
}

- (void)setupForMailSend {

    self.mailComposeViewController = [[MFMailComposeViewController alloc] init];
    self.mailComposeViewController.mailComposeDelegate = self;
}

- (void)loadData {

    if (!self.currentItem) {

        self.noContentView.hidden = NO;
        return;
    }

    self.noContentView.hidden = YES;
    self.titleLabel.text = self.currentItem.name;

    NSURL *imageURL = [[NSURL alloc] initWithString:self.currentItem.fullImageURL];
    [[NetworkManager sharedManager] loadDataFromURL:imageURL completion:^(NSData *data, NSError *error) {

        UIImage *image = [[UIImage alloc] initWithData:data];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (image) {
                [self.imageView setImage:image];
            } else {
                [self.imageView setImage:[UIImage imageNamed:@"NoImage"]];
            }
        });
    }];
}

#pragma mark - IB Action

- (IBAction)shareButtonTap:(UIBarButtonItem *)sender {

    if([MFMailComposeViewController canSendMail]) {

        [self setupForMailSend];
        [self.mailComposeViewController setSubject:@"Image"];
        UIImage *currentImage = self.imageView.image;
        //TODO: convert image to data
        //TODO: set mime type
        [self.mailComposeViewController addAttachmentData:currentImage mimeType:@"" fileName:[NSString stringWithFormat:@"%@.jpg", self.currentItem.name]];
        [self presentViewController:self.mailComposeViewController animated:YES completion:nil];
    }
    else {
        //Show message 'Can`t send email'
    }
}

#pragma mark - Mail Compose View Controller Delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(nullable NSError *)error
{

    [controller dismissViewControllerAnimated:YES completion:nil];
}


@end
