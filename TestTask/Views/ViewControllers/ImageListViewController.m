//
//  ImageListViewController.m
//  TestTask
//
//  Created by Nikolay on 02.06.17.
//  Copyright © 2017 Nikolay. All rights reserved.
//

#import "ImageListViewController.h"
#import "Item.h"
#import "ImageTableViewCell.h"
#import "ImageDetailViewController.h"

@interface ImageListViewController ()

@property (copy, nonatomic) NSArray *imageList;

@end

@implementation ImageListViewController

#pragma mark - Life cycle

- (void)viewDidLoad {

    [super viewDidLoad];
    [self setupTableView];
    [self loadImages];
}

- (void)setupTableView {

    [self.tableView registerClass:[ImageTableViewCell class] forCellReuseIdentifier:@"imageCellID"];
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(loadImages) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.imageList = @[];
}

- (void)loadImages {

    if (!self.refreshControl.refreshing) {
        //Show activity indicator
    }

    [Item imageListWithCompletion:^(NSArray *imageList, NSError *error) {

        if (!self.refreshControl.refreshing) {
            //Hide activity indicator
        }
        else {
            [self.refreshControl endRefreshing];
        }

        if (error || !imageList || imageList.count == 0) {

            [self showNoContentView];
        } else {
            self.imageList = imageList;
            [self.tableView reloadData];
        }
    }];
}

- (void)showNoContentView {

    if (self.tableView.backgroundView)
        return;

    UILabel *noContentLabel = [[UILabel alloc] init];
    noContentLabel.text = @"Error loading data";
    noContentLabel.textAlignment = NSTextAlignmentCenter;
    [noContentLabel sizeToFit];
    noContentLabel.center = CGPointMake(self.tableView.bounds.size.width / 2, self.tableView.bounds.size.height / 2);
    self.tableView.backgroundView = noContentLabel;
}


- (void)viewWillAppear:(BOOL)animated {

    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
}

#pragma mark - Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.imageList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    ImageTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"imageCellID" forIndexPath:indexPath];
    Item *item = self.imageList[indexPath.row];
    [cell configureWithItem:item];

    return cell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [self performSegueWithIdentifier:@"showDetail" sender:indexPath];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"showDetail"]) {

        NSIndexPath *indexPath = sender;
        Item *selectedItem = self.imageList[indexPath.row];
        UINavigationController *navigationController = segue.destinationViewController;
        ImageDetailViewController *imageDetailViewController = (ImageDetailViewController*)navigationController.topViewController;
        imageDetailViewController.currentItem = selectedItem;
        imageDetailViewController.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        imageDetailViewController.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

@end
