//
// Created by Nikolay on 24.05.17.
// Copyright (c) 2017 Nikolay. All rights reserved.
//

#import "NetworkManager.h"


@implementation NetworkManager

+ (instancetype)sharedManager
{
    static dispatch_once_t once;
    static NetworkManager *sharedManager;

    dispatch_once(&once, ^
    {
        sharedManager = [[NetworkManager alloc] init];
    });

    return sharedManager;
}

- (void)loadDataFromURL:(NSURL *)url completion:(void (^)(NSData *data, NSError *error))completion {

    NSURLSession *session = [NSURLSession sharedSession];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {

        completion(data, error);
    }];
    [task resume];
}

@end
