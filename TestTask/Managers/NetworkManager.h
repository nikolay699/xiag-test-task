//
// Created by Nikolay on 24.05.17.
// Copyright (c) 2017 Nikolay. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NetworkManager : NSObject

+ (instancetype)sharedManager;

- (void)loadDataFromURL:(NSURL *)url completion:(void (^)(NSData *data, NSError *error))completion;

@end
