//
// Created by Nikolay on 02.06.17.
// Copyright (c) 2017 Nikolay. All rights reserved.
//

#import "Item.h"
#import "NetworkManager.h"

static NSString* const defaultURL = @"http://www.xiag.ch/share/testtask/list.json";

@implementation Item

+ (void)imageListWithCompletion:(void(^)(NSArray *imageList, NSError *error))completion {

//    NSString *pathToFile = [[NSBundle mainBundle] pathForResource:@"xiag_list" ofType: @"json"];
//    NSData *jsonData = [[NSData alloc] initWithContentsOfFile:pathToFile];

    NSDate *jsonData = nil;

    NSURL *url = [[NSURL alloc] initWithString:defaultURL];
    [[NetworkManager sharedManager] loadDataFromURL:url completion:^(NSData *data, NSError *error) {

        NSArray *itemArray = [Item itemArrayWithData:data];

        completion(itemArray, error);
    }];
}

+ (NSArray *)itemArrayWithData:(NSData *)data {

    if (!data) {
        return nil;
    }

    NSMutableArray *resultArray = [[NSMutableArray alloc] init];

    NSError *jsonError;

    id object = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];

    if ([object isKindOfClass:[NSArray class]]) {

        NSArray *jsonArray = object;
        for (NSDictionary *jsonDictionary in jsonArray) {

            Item *item = [[Item alloc] init];
            item.name = jsonDictionary[@"name"];
            item.thumbnailImageURL = jsonDictionary[@"url_tn"];
            item.fullImageURL = jsonDictionary[@"url"];

            [resultArray addObject:item];
        }
    }

    return resultArray;
}

@end