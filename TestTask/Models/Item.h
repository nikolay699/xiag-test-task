//
// Created by Nikolay on 02.06.17.
// Copyright (c) 2017 Nikolay. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Item : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *thumbnailImageURL;
@property (nonatomic, copy) NSString *fullImageURL;

+ (void)imageListWithCompletion:(void(^)(NSArray *imageList, NSError *error))completion;

@end